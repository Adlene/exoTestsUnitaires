"use strict"
const Comment = require("./Comment");

class Person {
    constructor (name, surname){
        this.name = name;
        this.surname = surname;
        this.comment = new Comment();
    }
}

module.exports = Person;