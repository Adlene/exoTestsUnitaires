"use strict";

class Comment {
    constructor(value = null) {
        this.value = value;
    }
    get(){
        return this.value;
    }
    set(value){
        this.value = value;
        return this.value;
    }
}

module.exports = Comment;