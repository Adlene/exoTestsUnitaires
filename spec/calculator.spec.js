"use strict";

describe("Calculator", function() {
    //récupère l'export de la class Calculator
    const Calculator = require("../Calculator.js");
    let calculator;

    beforeEach(function() {
       calculator = new Calculator();
    });

    it("should add two numbers", function() {
        let result = calculator.add(1, 1);
      expect(result).toBe(2);
    });
    it("should substract two numbers", function(){
        let result = calculator.sub(1, 1);
     expect(result).toBe(0);
    })
  });