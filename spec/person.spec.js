"use strict";

describe("Person", function () {
    //récupère l'export de la class Person et Comment
    const Person = require("../Person");
    const Comment = require("../Comment");

    let person, value;

    beforeEach(function () {
        person = new Person("toto", "l'asticot");

        spyOn(person.comment, "get").and.returnValue(value);
        spyOn(person.comment, "set").and.callFake((val) => {
            val =  value;
            return value;
        });
    });

    it("should set a comment", function () {
        person.comment.set("this is a comment");
        expect(person.comment.get()).toBe("this is a comment");
    });
    it("should get a comment", function () {
        person.comment.set("this is a comment");
        let com = person.comment.get();
        expect(com).toBe("this is a comment");
    });})